package com.com.telconet.demo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.com.telconet.demo.entities.Category;
import com.com.telconet.demo.repositories.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	CategoryRepository categoryRepository;

	public Category editCategory(Category category) {
		
		//Consulta en la db la existencia de la categoria
		Optional<Category> categoryData = categoryRepository.findById(category.getId());

		//Valida la existencia de la categoria para dar la respuesta
		if (categoryData.isPresent()) {
			categoryData.get().setId(category.getId());
			categoryData.get().setDescription(category.getDescription());
			categoryData.get().setName(category.getName());

			return categoryRepository.save(categoryData.get());
		} else {
			return categoryRepository.save(category);
		}
		
	}

	public Category saveCategory(Category category) {
		
		return categoryRepository.save(category);
	}

	public Boolean deleteCategory(Integer id) {
		
		Optional<Category> exist = categoryRepository.findById(id);

		//Valida la existencia de la categoria para dar la respuesta
		if (exist.isPresent()) {
			return false;
		} else {
			categoryRepository.deleteById(id);
			return true;
		}
	}

	public List<Category> listCategory() {
		
		//Obtiene todas las categorias existentes dentro de la db
		return categoryRepository.findAll();
	}
	
	public Category findByCategory(Integer id) {
		
		//Obtiene las categorias existentes en base al id suministrado
		return categoryRepository.findById(id).get();
	}

}
