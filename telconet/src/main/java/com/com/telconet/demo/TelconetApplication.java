package com.com.telconet.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelconetApplication {

	public static void main(String[] args) {
		SpringApplication.run(TelconetApplication.class, args);
	}

}
