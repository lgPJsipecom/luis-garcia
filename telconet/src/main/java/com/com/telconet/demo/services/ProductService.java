package com.com.telconet.demo.services;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.com.telconet.demo.entities.Product;
import com.com.telconet.demo.repositories.ProductRepository;


@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;

	@Autowired(required = true)
	private RestTemplate rest;

	@Value("${PATH.URL.LOAD}")
	String urlConsumo;
	
	/**
	 * 
	 * @return
	 * @throws Exception 
	 */
	public String loadProduct() throws Exception {
		
		//Metodo para comunicacion con servicio de carga de productos
		productRepository.saveAll(loadProductMasive()) ;
		return "cargados";	
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Boolean deleteProduct(Integer id) {

		//Consulta en la db la existencia del producto
		Optional<Product> exist = productRepository.findById(id);

		//Valida la existencia del producto para dar la respuesta
		if (exist.isPresent()) {
			productRepository.deleteById(id);
			return true;
		} else {
			return false;
		}

	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Product findProductId(Integer id) {

		//Consulta en la db la existencia del producto
		Optional<Product> exisProduct = productRepository.findById(id);

		//Valida la existencia del producto para dar la respuesta
		if (exisProduct.isPresent()) {
			return exisProduct.get();
		} else {
			return exisProduct.orElseThrow();
		}
	}

	/**
	 * Metodo encargado de editar productos 
	 * @param product
	 * @return
	 */
	public Product editProduct(Product product) {

		//Consulta en la db la existencia del producto
		Optional<Product> productData = productRepository.findById(product.getId());

		//Valida si existe, si esta presente edita el producto
		if (productData.isPresent()) {
			productData.get().setId(product.getId());
			productData.get().setTitle(product.getTitle());
			productData.get().setDescription(product.getDescription());
			productData.get().setPrice(product.getPrice());

			return productRepository.save(productData.get());
		} else {
			//No esta presente guarda el producto
			return productRepository.save(product);
		}
	}

	/**
	 * Metodo encargado de editar imagen del producto, para asi no afectar toda la estructura
	 * @param id
	 * @param image
	 * @return
	 */
	public Product editImageProduct(Integer id, String image) {

		//Consulta en la db la existencia del producto
		Optional<Product> changeImage = productRepository.findById(id);

		//Valida si existe, si esta presente edita el producto
		if (changeImage.isPresent()) {

			changeImage.get().setId(id);
			changeImage.get().setImagen(image);
			return productRepository.save(changeImage.get());

		} else {
			return changeImage.orElseThrow();
		}
	}
	
	
	//Metodo encargado de la conexion al servicio rest que obtiene el json de productos
	private  List<Product> loadProductMasive() {
		rest = new RestTemplate();
		 ResponseEntity<Product[]> response =
				 rest.getForEntity(
						 urlConsumo,
						 Product[].class);
		 Product[] prod = response.getBody();
              List<Product> m = Arrays.asList(prod);
      return  m;
	}
		

}
