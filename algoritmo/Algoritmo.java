package Executor;

import java.util.Scanner;

public class Algoritmo {

    public static void main(String[] args) {

        //Funcion encargada de recibir los datos de consola
        Scanner data = new Scanner(System.in);

        //frase a ser invertida
        System.out.print("Ingrese frase para ser invertida: ");
        String frase = data.nextLine();

        //palabra para ser colocada en espacios de separacion
        System.out.print("Ingrese palabra para ser colocada en separaciones: ");
        String palabra = data.nextLine();

        //Variable local para almacenar frase invertida
        String cadenaInvertida = "";


        for (int x = frase.length() - 1; x >= 0; x--){
            cadenaInvertida = cadenaInvertida + frase.charAt(x);
        }
        //Funcion para reemplazar los campos vacios
        System.out.println(cadenaInvertida.replace(" ", palabra));
    }
}
